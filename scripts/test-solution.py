#!/usr/bin/env python3

import logging
import os
import shlex
from subprocess import Popen, PIPE, TimeoutExpired
import sys
import yaml
from utils import VerifyWorker


COMPS = {2: '18.07',
         3: '18.09',
         4: '18.10',
         5: '18.11',
         6: '19.01',
         7: '19.02',
         8: '19.03',
         9: '19.05',
        10: '19.06'}

logging.basicConfig(format='%(levelname)s:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def parse_info(yaml_file):
    """
    Parse info.yaml for the current competition, return parsed yaml object
    """
    if not os.path.isfile(yaml_file):
        raise RuntimeError("Missing info.yaml file: {}".format(yaml_file))

    info = yaml.load(open(yaml_file))
    return info


def test_run(challenge, base_dir, input_file=None):
    """
    Run the program on the sample input - Just useful to make sure everything is working
      (note programs may have no output with sample inputs)
    """

    source_dir = os.path.join(base_dir, 'source', challenge["install_dir"], 'lava-install')
    local_dir = os.path.join(base_dir, 'download', challenge["install_dir"])
    library_dir = None
    if "library_dir" in challenge.keys():
        library_dir = os.path.join(local_dir, challenge["library_dir"])
    binary = os.path.join(source_dir, challenge["binary_path"])
    if input_file is None:
        input_file = os.path.join(local_dir, challenge["sample_inputs"][0])
    args = challenge["binary_arguments"].format(input_file=input_file, install_dir=local_dir)
    if library_dir:
        command = "LD_LIBRARY_PATH={library_dir} {binary} {args}".format(library_dir=library_dir, binary=binary, args=args)
    else:
        command = "{binary} {args}".format(binary=binary, args=args)
    logger.info("Executing: %s", command)
    args = shlex.split(command)
    p = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    r = {'returncode': 0, 'bug_id': None, 'src_line': "", 'match': False}
    try:
        sout, serr = p.communicate(timeout=5)
    except TimeoutExpired:
        logger.error("timeout expired for: %s", input_file)
        return r
    lava = serr.find(b'LAVALOG:')
    if lava > 0:
        msg = serr[lava:].split(maxsplit=3)
        r['returncode'] = p.returncode
        r['bug_id'] = msg[1].strip()[:-1].decode('utf-8')
        r['src_line'] = msg[2].split(b'\n')[0].strip().decode('utf-8')
        r['match'] = (r['bug_id'] == os.path.basename(input_file).split('.')[0])
        if not r['match']:
            logger.error(r)
    else:
        logger.error("STDOUT: %s\n\tSTDERR: %s\n",
                     sout.decode('utf-8', 'backslashreplace'),
                     serr.decode('utf-8', 'backslashreplace'))
    return r


def run_tests(info):
    base_dir = COMPS[info['rode0day_id']]
    results = list()
    for challenge_name, challenge in info['challenges'].items():
        # test_run(challenge, base_dir)
        vw = VerifyWorker(challenge, base_dir)
        solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['challenge_id']))
        if not os.path.isdir(solutions_dir):
            solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['install_dir']))
        for solution in os.listdir(solutions_dir):
            sol_file = os.path.join(solutions_dir, solution)
            results.append(vw.run_binary(sol_file))
    validated = [1 for r in results if r['match']]
    logger.info("DONE: %d  of %d bugs validated.", len(validated), len(results))


def run_test(info, challenge_name):
    base_dir = COMPS[info['rode0day_id']]
    results = list()
    challenge = info['challenges'][challenge_name]
    vw = VerifyWorker(challenge, base_dir)
    # test_run(challenge, base_dir)

    solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['challenge_id']))
    if not os.path.isdir(solutions_dir):
        solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['install_dir']))
    for solution in os.listdir(solutions_dir):
        sol_file = os.path.join(solutions_dir, solution)
        results.append(vw.run_binary(sol_file))
    validated = [1 for r in results if r['match']]
    logger.info("DONE: %d  of %d bugs validated.", len(validated), len(results))


def main(args):
    if len(args) < 2 or len(args) > 3:
        print("Usage {} <info.yaml> [ <challenge_name> ]".format(args[0]))
        return 0
    info = parse_info(args[1])
    if len(args) == 3:
        run_test(info, args[2])
    elif len(args) == 2:
        run_tests(info)


if __name__ == "__main__":
    main(sys.argv)
    sys.exit(0)
