import glob
import hashlib
import io
import json
import sys
import os
import logging
import shlex
import tempfile
import requests
from subprocess import run, Popen, PIPE, DEVNULL, TimeoutExpired
import drcov
try:
    from minio import Minio
    from minio.error import ResponseError
except ImportError:
    pass
# from google.cloud import storage


DRRUN = '/opt/dr/bin/drrun'
DR2LCOV = '/opt/dr/bin/drcov2lcov'
GC_BUCKET = 'crashes.fuzz.tr0gd0r.com'
MC_BUCKET = 'crashes'
S3_BUCKET = 'crashes.fuzz.tr0gd0r.com'

# export GOOGLE_APPLICATION_CREDENTIALS="/home/user/Downloads/[FILE_NAME].json"

logging.basicConfig(format='%(levelname)s:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def initialize_clients():
    from creds import MC, S3, GC
    mc_client = {'client': Minio(**MC),
                 'bucket': MC_BUCKET}
    s3_client = {'client': Minio(**S3),
                 'bucket': S3_BUCKET}
    gc_client = {'client': Minio(**GC),
                 'bucket': GC_BUCKET}
    return [mc_client, s3_client, gc_client]


def mc_upload_file(client, src_filename, dst_filename):
    cl = client['client']
    bucket = client['bucket']
    try:
        cl.fput_object(bucket, dst_filename, src_filename)
    except ResponseError as err:
        logger.error(err)


def mc_upload_object(client, data, dst_filename):
    cl = client['client']
    bucket = client['bucket']
    io_data = io.BytesIO(data.encode())
    try:
        cl.put_object(bucket, dst_filename, io_data, len(data))
    except ResponseError as err:
        logger.error(err)


def safe_json(text):
    try:
        j = json.loads(text)
    except ValueError:
        print("Invalid response from server:\n\t{}".format(text))
        sys.exit(1)
        return {'success': False}
    return j


def sha1sum(file_path):
    with open(file_path, 'rb') as f:
        m = hashlib.sha1()
        m.update(f.read())
        return m.hexdigest()


class VerifyWorker(object):

    def __init__(self, challenge, base_dir, base_url=None, online=False):
        self.challenge = challenge
        self.install_dir = challenge['install_dir']
        self.binary_path = challenge['binary_path']
        self.base_dir = base_dir
        self.base_url = base_url
        self.crashes = list()

        source_dir = os.path.join(base_dir, 'source', self.install_dir, 'src')
        source_dir = source_dir if os.path.isdir(source_dir) else None
        self.source_dir = source_dir

        if online:
            from creds import HDR
            self.auth_header = HDR
            self.clients = initialize_clients()
        else:
            self.clients = list()

    def update_crash_info(self):
        data = list()
        url = self.base_url + '/api/crash/update'
        for crash in self.crashes:
            new_additional = "{}\n".format(crash['additional'].strip())
            new_additional += "{}\n".format(crash['cov_stats'])
            form = {'stat_id': crash['stat_id'],
                    'crash_hash': crash['crash_hash'],
                    'bb_count': crash['cov_stats']['bb_a'],
                    'bb_counts': crash['cov_stats'],
                    'additional': new_additional}
            if 'GracefulExit' in crash['short_description']:
                form['verified'] = -1
            elif crash['classification'] == 'EXPLOITABLE':
                form['verified'] = 9
            else:
                form['verified'] = 2
            if 'bug_id' in crash:
                form['bug_id'] = crash['bug_id']
            form['exploitability'] = crash['classification']
            form['stack_hash'] = crash['hash']
            form['additional'] += "\n\t{}\n".format(crash['short_description'])
            form['additional'] += "{}\n".format(crash['instruction'])
            form['additional'] += "{}\n".format("\n".join(crash['stack_frame']))
            data.append(form)
        r = requests.post(url, headers=self.auth_header, json=data)
        j = safe_json(r.text)
        logger.info(j['message'])


    def get_crashes(self, limit=10):
        local_dir = os.path.join(self.base_dir, 'download', self.install_dir)
        target = os.path.join(local_dir, self.binary_path)
        if not os.path.isfile(target):
            print("ERROR: target file not found: {}".format(target))
            return None
        target_hash = sha1sum(target)
        url = self.base_url + '/api/crash/verify'
        form = {'target_hash': target_hash, 'limit': limit}
        r = requests.post(url, headers=self.auth_header, data=form)
        j = safe_json(r.text)
        if not j['success']:
            logger.error(j['message'], form)
            return None
        crashes = j['data']
        return crashes

    def process_crashes(self, limit=10):
        self.crashes = self.get_crashes(limit)
        for crash in self.crashes:
            url = self.base_url + '/api/crash/download/{}'.format(crash['crash_id'])
            r = requests.get(url, headers=self.auth_header)
            # cd = r.headers.get('Content-Disposition')
            # matches = re.findall('filename="(\S+)"',cd)
            # filename = matches[0] if len(matches) else None
            with open('.current_input', 'wb') as f:
                f.write(r.content)
            logger.info("\n\tcrash_id: %s", crash['crash_id'])
            self.run_exploitable(crash)
            crash['cov_stats'] = self.run_with_dynamorio(crash)
            if self.source_dir:
                crash.update(self.run_binary('.current_input'))
        self.update_crash_info()

    def get_target_command(self, input_file, lavalog=False):
        source_dir = os.path.join(self.base_dir, 'source', self.install_dir, 'lava-install')
        local_dir = os.path.join(self.base_dir, 'download', self.install_dir)
        if lavalog and os.path.isdir(source_dir):
            binary = os.path.join(source_dir, self.binary_path)
        else:
            binary = os.path.join(local_dir, self.binary_path)
        library_dir = None
        if "library_dir" in self.challenge.keys():
            library_dir = os.path.join(local_dir, self.challenge["library_dir"])
        args = self.challenge["binary_arguments"].format(input_file=input_file, install_dir=local_dir)
        if library_dir:
            command = "LD_LIBRARY_PATH={} {} {}".format(library_dir, binary, args)
        else:
            command = "{binary} {args}".format(binary=binary, args=args)
        return shlex.split(command)


    def get_coverage_stats(self, cov, module_name):
        mod_bb = cov.get_blocks_by_module(module_name)
        return {'bb_a': len(cov.basic_blocks),
                'bb_u': len(set(bb.start for bb in cov.basic_blocks)),
                'bb_ma': len(mod_bb),
                'bb_mu': len(set(bb.start for bb in mod_bb))}

    def run_with_dynamorio(self, crash):
        tgt_cmd = self.get_target_command('.current_input', lavalog=self.source_dir is not None)

        with tempfile.TemporaryDirectory() as out:
            args = [DRRUN, '-t', 'drcov', '-logdir', out, '--']+tgt_cmd
            logger.info("%s", " ".join(args))
            p = run(args, stdout=DEVNULL, stderr=DEVNULL)
            files = glob.glob("{}/*.log".format(out))
            cov = drcov.DrcovData(files[0]) if files else None
            if cov is None:
                logger.error("ERROR: failed to get coverage")
                return None
            binary = os.path.split(self.binary_path)[-1]
            stats = self.get_coverage_stats(cov, binary)
            root_dir = self.install_dir + "_" + crash['target_hash'][:7]
            upload_dir = os.path.join(root_dir, 'cov', crash['crash_hash'])
            for client in self.clients:
                mc_upload_file(client, files[0], os.path.join(upload_dir, 'drcov.log'))

            if self.source_dir:
                cov_file = os.path.join(out, 'coverage.info')
                args = [DR2LCOV, '-dir', out, '-output', cov_file,
                        '-mod_filter', binary, '-src_filter', self.source_dir]
                logger.info("%s", ' '.join(args))
                p = run(args, stdout=DEVNULL, stderr=DEVNULL)
                for client in self.clients:
                    mc_upload_file(client, cov_file, os.path.join(upload_dir, 'coverage.info'))
            else:
                logger.error("NO SOURCE: %s", self.install_dir)
        return stats

    def run_exploitable(self, crash):
        tgt_cmd = self.get_target_command('.current_input')
        gdb_cmd = ["gdb", "--batch", "-ex", "run", "-ex", "exploitable -m ", "--args"]
        gdb_cmd += tgt_cmd
        logger.info("%s", " ".join(gdb_cmd))
        p = run(gdb_cmd, stdout=PIPE)
        data = None
        for line in p.stdout.splitlines():
            if b"classification" in line:
                data = line.decode(encoding="utf-8", errors="ingore")
        if data is None:
            return None
        root_dir = self.install_dir + "_" + crash['target_hash'][:7]
        upload_filename = os.path.join(root_dir, 'cov',
                                       crash['crash_hash'],
                                       'exploitable.json')
        for client in self.clients:
            mc_upload_object(client, data, upload_filename)
        jdata = json.loads(data)
        logger.info("\t: %(classification)s\t %(short_description)s", jdata)
        crash.update(jdata)
        return jdata

    def run_binary(self, input_file=None):
        """
        Run the program on the sample input - Just useful to make sure everything is working
          (note programs may have no output with sample inputs)
        """

        tgt_cmd = self.get_target_command(input_file, lavalog=True)
        # args = shlex.split(tgt_cmd)
        local_dir = os.path.join(self.base_dir, 'download', self.challenge["install_dir"])
        if input_file is None:
            input_file = os.path.join(local_dir, self.challenge["sample_inputs"][0])
        logger.info("Executing: %s\t", ' '.join(tgt_cmd))
        p = Popen(tgt_cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        r = {'returncode': 0, 'bug_id': None, 'src_line': "", 'match': False}
        try:
            sout, serr = p.communicate(timeout=5)
        except TimeoutExpired:
            logger.error("timeout expired for: %s", input_file)
            return r
        r['returncode'] = p.returncode
        lava = serr.find(b'LAVALOG:')
        if lava > 0:
            msg = serr[lava:].split(maxsplit=3)
            r['bug_id'] = msg[1].strip()[:-1].decode('utf-8')
            r['src_line'] = msg[2].split(b'\n')[0].strip().decode('utf-8')
            r['match'] = (r['bug_id'] == os.path.basename(input_file).split('.')[0])
            if not r['match']:
                logger.error(r)
        else:
            logger.error("STDOUT: %s\n\tSTDERR: %s\n",
                         sout.decode('utf-8', 'backslashreplace'),
                         serr.decode('utf-8', 'backslashreplace'))
        return r
